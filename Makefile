CC=gcc
OBJECT=db

default: main

main:
	$(CC) $@.c -o $(OBJECT)

clean:
	rm -f $(OBJECT)
